# Setting Up Local Environment

In this section we will setup the local environment in order to run, test and lint the "Calculator" app locally.

**You will need:**
- [Git](https://git-scm.com/) installed
- Python, Pip and VirtualEnv installed

**Useful links**
- [Installing Git {Linux, Windows and MacOS}](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Installing Python 3 {Linux, Windows and MacOS}](https://wiki.python.org/moin/BeginnersGuide/Download)
- Installing pip3 {Debian}: `$ sudo apt install python3-pip`
- Installing python3-venv {Debian}: `$ apt-get install python3-venv`

> **NOTE:** These following instructions are assuming that you are using Unix (Debian) system.

## 1 - Create a [virtual environment](https://docs.python.org/3/library/venv.html)

```
$ cd sinfo-2022-workshop
$ python3 -m venv env
```

## 2 - Activate the virtual environment

```
$ source env/bin/activate
```

## 3 - Install python dependencies

```
$ pip install -r requirements.txt
```

## 4 - Run the application locally

```
$ export FLASK_APP=calculator.py
$ export FLASK_ENV=development
$ flask run -p 1234
```

## 5 - Access the application

Application is available on http://127.0.0.1:1234/

## 6 - Test the application locally

```
$ pytest
```

## 7 - Lint the application locally

```
$ pylint *.py
```

## 8 - Deploy to Heroku from local

Export the API key, and App name

```
$ export HEROKU_API_KEY=<API_KEY>
$ export HEROKU_APP_NAME=<APP_NAME>
```

Add the `heroku` git remote:

```
$ git remote add heroku https://heroku:${HEROKU_API_KEY}@git.heroku.com/${HEROKU_APP_NAME}.git
```

Push to `heroku` remote to deploy the app:

```
$ git push -f heroku HEAD:master
```
